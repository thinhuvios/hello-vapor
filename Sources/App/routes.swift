import Vapor
import Authentication

public func routes(_ router: Router) throws {
    // `User` routing
    let user = UserController(router: router)
    let product = ProductController(router: router)
    
    router.register(user)
    router.register(product)
}


extension Router {
    /// Registers all of the routes in the group to this router.
    ///
    /// - parameters:
    ///     - collection: `RouteCollection` to register.
    func register(_ controller: ControllerProtocol) {
        controller.registerRoutes()
    }
}
