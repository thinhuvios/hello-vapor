//
//  User.swift
//  App
//
//  Created by MBP0019 on 8/28/18.
//

import Foundation
import Vapor
import MySQL
import FluentMySQL
import Authentication

final class User: MySQLModel {
    static var entity: String = "users"
    var id: Int?
    var first_name: String
    var last_name: String
    var mail: String
    var password: String
    var tokens: Children<User, UserToken> {
        return children(\.userID)
    }
    init(id: Int? = nil, first_name: String, last_name: String, mail: String) {
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.mail = mail
        self.password = ""
    }
}

extension User: Model {}
extension User: Content {}
extension User: MySQLMigration {}
extension User: Parameter {}
extension User: SessionAuthenticatable {}

extension User: TokenAuthenticatable {
    typealias TokenType = UserToken
}
