//
//  UserExt.swift
//  App
//
//  Created by MBP0019 on 9/6/18.
//

import Foundation
import Vapor
import FluentMySQL
import Authentication
import Random

struct SignupRequest: Content {
    var first_name: String
    var last_name: String
    var mail: String
    var password: String
}

struct LoginRequest: Content {
    var mail: String
    var password: String
}

struct LoginResponse: Content {
    var access_token: String
    var mail: String
}

struct SignupResponse: Content {
    var first_name: String
    var last_name: String
    var mail: String
    var access_token: String
    
    init(user: User, access_token: String) {
        self.first_name = user.first_name
        self.last_name = user.last_name
        self.mail = user.mail
        self.access_token = access_token
    }
}

struct PublicUser: Content {
    var id: Int?
    var first_name: String
    var last_name: String
    var mail: String
    
    init(user: User) {
        self.id = user.id
        self.first_name = user.first_name
        self.last_name = user.last_name
        self.mail = user.mail
    }
}

struct UserToken: Model {
    static var entity = "user_tokens"
    static var idKey: WritableKeyPath<UserToken, User.ID?> {
        return \.id
    }
    
    typealias Database = User.Database
    
    typealias ID = User.ID
    
    var id: Int?
    var string: String
    var userID: User.ID
    var active: Bool
    var user: Parent<UserToken, User> {
        return parent(\.userID)
    }
    init(id: Int? = nil, string: String, userId: User.ID, active: Bool = false) {
        self.id = id
        self.string = string
        self.userID = userId
        self.active = active
    }
    
    static func createToken(forUser user: User) throws -> UserToken {
        let tokenString = try Helpers.randomToken(withLength: 60)
        let newToken = try UserToken(string: tokenString, userId: user.requireID(), active: true)
        return newToken
    }
}

extension UserToken: Content {}
extension UserToken: MySQLMigration {}
extension UserToken: Parameter {}
extension UserToken: SessionAuthenticatable {}

extension UserToken: BearerAuthenticatable {
    static var tokenKey: WritableKeyPath<UserToken, String> {
        return \.string
    }
}

extension UserToken: Token {
    typealias UserType = User // 2
    typealias UserIDType = User.ID //3
    static var userIDKey: WritableKeyPath<UserToken, User.ID> {
        return \.userID
    }
}

class Helpers {
    class func randomToken(withLength length: Int) throws -> String {
        return "$!abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let allowedChars = "$!abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let allowedCharsCount = UInt32(allowedChars.count)
        var randomString = ""
        for _ in 0 ..< length {
            let randomNumber = try OSRandom().generate(Int.self) // Int(arc4random_uniform(allowedCharsCount))
            let randomIndex = allowedChars.index(allowedChars.startIndex, offsetBy: randomNumber)
            let newCharacter = allowedChars[randomIndex]
            randomString += String(newCharacter)
        }
        return randomString
    }
}

struct UpdateProfileRequest: MySQLModel, Content, Model {
    var id: Int?
    var first_name: String?
    var last_name: String?
    var mail: String?
}

extension UpdateProfileRequest: Parameter {}
