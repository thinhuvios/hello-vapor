//
//  Product.swift
//  App
//
//  Created by MBP0019 on 9/8/18.
//

import Foundation
import Vapor
import FluentMySQL

final class Product: MySQLModel {
    static var entity = "products"
    var id: Int?
    var name: String
    var description: String
    var price: Double
    var category_id: Int?
    var created: Date?
    var modified: Date?
    
    init(id: Int? = nil, name: String, description: String, price: Double) {
        self.id = id
        self.name = name
        self.description = description
        self.price = price
    }
}

extension Product: Model {}
extension Product: Content {}
extension Product: MySQLMigration {}
extension Product: Parameter {}

struct EditProduct: Content {
    
}
