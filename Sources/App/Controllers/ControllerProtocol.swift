//
//  ControllerProtocol.swift
//  App
//
//  Created by MBP0019 on 9/6/18.
//

import Foundation
import Vapor

protocol ControllerProtocol {
    // Dependencies Injection
    var router: Router { set get }
    init(router: Router)
    
    // Functions
    func registerRoutes()
}
