//
//  UserController.swift
//  App
//
//  Created by MBP0019 on 8/29/18.
//

import Foundation
import Vapor
import FluentMySQL
import Crypto

final class UserController: ControllerProtocol {
    typealias Output = PublicUser
    
    var router: Router
    
    init(router: Router) {
        self.router = router
    }
    
    func registerRoutes() {
        router.post("users", "signup", use: signup)
        router.post("users", "login", use: login)
        let token = User.tokenAuthMiddleware()
        let authenSessionRouter = router.grouped(token)
        authenSessionRouter.get("users", "profile", use: profile)
        authenSessionRouter.get("users", use: index)
        authenSessionRouter.get("users", "logout", use: logout)
        authenSessionRouter.get("users", Int.parameter, use: info)
        authenSessionRouter.patch("users", UpdateProfileRequest.parameter, use: update)
    }
    
    /// Returns a list of `User`s.
    func index(_ req: Request) throws -> Future<[Output]> {
        guard let _ = try req.authenticated(UserToken.self) else { throw Abort(.unauthorized) }
        let all = User.query(on: req).all()
        return all.map { (users) -> [Output] in
            let publicUsers: [PublicUser] = users.map { PublicUser(user: $0) }
            return publicUsers
        }
        
    }
    
    /// Login
    func login(_ req: Request) throws -> Future<LoginResponse> {
        let decoded = try req.content.decode(LoginRequest.self)
        return decoded.flatMap { loginRequest in
            // Validate
            return User.query(on: req).filter(\User.mail == loginRequest.mail).first().flatMap { result in
                guard let result = result else {
                    throw Abort(.notFound, reason: "Email does not exist", identifier: "users.login.error")
                }
                
                let accessToken = try UserToken.createToken(forUser: result)
                return accessToken.save(on: req).map(to: LoginResponse.self) { token in
                    return LoginResponse(access_token: token.string, mail: result.mail)
                }
            }
        }
    }
    
    /// Sign up
    func signup(_ req: Request) throws -> Future<SignupResponse> {
        let decoded = try req.content.decode(SignupRequest.self)
        return decoded.flatMap { signupRequest in
            return User.query(on: req).filter(\User.mail == signupRequest.mail).first().flatMap { result in
                if let _ = result {
                    throw Abort(.badRequest, reason: "Email existed", identifier: "users.signup.error.enail")
                } else {
                    let newUser = User(first_name: signupRequest.first_name,
                                       last_name: signupRequest.last_name,
                                       mail: signupRequest.mail)
                    let passwordHashed = try req.make(BCryptDigest.self).hash(signupRequest.password)
                    newUser.password = passwordHashed
                    return newUser.save(on: req).map(to: SignupResponse.self) { user in
                        let accessToken = try UserToken.createToken(forUser: user)
                        _ = accessToken.save(on: req)
                        return SignupResponse(user: user, access_token: accessToken.string)
                    }
                }
            }
        }
    }
    
    /// Profile
    func profile(_ req: Request) throws -> Future<Output> {
        guard let user = try req.authenticated(User.self) else { throw Abort(.unauthorized) }
        return Future.map(on: req, { () -> Output in
            return Output(user: user)
        })
    }
    
    /// User info
    func info(_ req: Request) throws -> Future<Output> {
        guard try req.isAuthenticated(User.self) else { throw Abort(.unauthorized) }
        let id = try req.parameters.next(Int.self)
        return User.find(id, on: req).map(to: Output.self) { (user) -> Output in
            guard let user = user else {
                throw Abort(.notFound)
            }
            return Output(user: user)
        }
    }
    
    /// Logout
    func logout(_ req: Request) throws -> HTTPStatus {
        guard try req.isAuthenticated(User.self) else { throw Abort(.unauthorized) }
        try req.unauthenticate(User.self)
        return .ok
    }
    
    /// Update profile
    func update(_ req: Request) throws -> Future<Output> {
        guard let user = try req.authenticated(User.self) else { throw Abort(.unauthorized) }
        return try req.content.decode(UpdateProfileRequest.self).flatMap { update in
            if let firstName = update.first_name {
                user.first_name = firstName
            }
            if let lastName = update.last_name {
                user.last_name = lastName
            }
            if let mail = update.mail {
                user.mail = mail
            }
            
            return user.save(on: req).map(to: PublicUser.self, { (usr) -> PublicUser in
                return PublicUser(user: usr)
            })
        }
    }
}

