//
//  ProductController.swift
//  App
//
//  Created by MBP0019 on 9/7/18.
//

import Foundation
import Vapor

final class ProductController: ControllerProtocol {
    var router: Router
    
    init(router: Router) {
        self.router = router
    }
    
    func registerRoutes() {
        let token = User.tokenAuthMiddleware()
        let authenSessionRouter = router.grouped(token)
        authenSessionRouter.get("products", use: index)
    }
    
    /// Returns a list of `Product`s.
    func index(_ req: Request) throws -> Future<[Product]> {
        guard let _ = try req.authenticated(UserToken.self) else { throw Abort(.unauthorized) }
        return Product.query(on: req).all()
    }
}
